import
  httpClient,
  json,
  std/[terminal],
  strformat,
  strutils,
  uri

import
  config,
  types


discard """

Invidious API reference:
https://github.com/iv-org/documentation/blob/master/API.md
"""

func makeUrl(videoId: string): string =
  "https://www.youtube.com/watch?v=" & videoId


proc getSearchResults*(searchQuery: string): SearchResults =
  # Using Invidious API to retrieve the search results but playing the results directly from YouTube.
  let queryParam = encodeUrl(searchQuery)
  let client = newHttpClient()
  client.headers = newHttpHeaders({"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36"})
  let response = get(client, &"{invidiousInstance}/api/v1/search?q={queryParam}")
  let jsonData = parseJson($response.body)
  if jsonData.kind == JObject: # Could be a 403 error
    stdout.styledWrite(fgRed, $response.body)
    quit(1)
  if len(jsonData) == 0:
    stdout.styledWrite(fgRed, "Got empty response from Invidious instance")
    quit(2)
  var searchResults: SearchResults = @[]
  for item in jsonData:
    if item["type"].getStr() == "video":
      searchResults.add((item["title"].getStr(), makeUrl(item["videoId"].getStr())))
    elif item["type"].getStr() == "playlist":
      searchResults.add((item["title"].getStr(), makeUrl(item["playlistId"].getStr())))
    # Not handling type = channel for now
  searchResults

proc getAutoPlayVideo*(searchResult: SearchResult): SearchResult =
  # Take a search result and fetch its first recommendation
  let videoId = searchResult.url.split("=")[1]
  let client = newHttpClient()
  let response = get(client, &"{invidiousInstance}/api/v1/videos/{videoId}")
  let jsonData = parseJson($response.body)
  let firstRecommendation = jsonData["recommendedVideos"][0]
  (firstRecommendation["title"].getStr(), makeUrl(firstRecommendation["videoId"].getStr()))
