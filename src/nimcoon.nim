import
  parseopt,
  strutils,
  tables

import
  config,
  lib,
  types,
  youtube


proc parseArguments(): CommandLineOptions =

  var
    searchQuery = ""
    options = to_table({
      "musicOnly": false,
      "feelingLucky": false,
      "fullScreen": false,
      "download": false,
      "nonInteractive": false,
      "autoPlay": false
    })

  # Non-interactive/Global options
  for kind, key, value in getopt():
    case kind
    of cmdArgument:
      searchQuery = key
    of cmdShortOption, cmdLongOption:
      case key
      of "m", "music": options["musicOnly"] = true
      of "l", "lucky": options["feelingLucky"] = true
      of "f", "full-screen": options["fullScreen"] = true
      of "d", "download": options["download"] = true
      of "n", "non-interactive": options["nonInteractive"] = true
      of "a", "auto-play": options["autoPlay"] = true
    of cmdEnd: discard

  if searchQuery == "":
    stderr.writeLine "Nimcoon doesn't permit browsing. You must provide a search query."
    quit(1)

  (searchQuery, options)


proc main() =
  let
    player = selectMediaPlayer()
    (searchQuery, options) = parseArguments()

  if(not isValidOptions(options)):
    quit(1)

  if searchQuery.startswith("http") or searchQuery.startswith("magnet"):
    if options["download"]:
      directDownload(sanitizeURL(searchQuery), options)
    else:
      directPlay(sanitizeURL(searchQuery), player, options)
    quit(0)

  # Take a shortcut and search directly with youtube-dl
  if options["feelingLucky"]:
    if options["download"]: luckyDownload(searchQuery, options)
    else: luckyPlay(searchQuery, player, options)
    quit(0)

  let searchResults = getSearchResults(searchQuery)
  if options["nonInteractive"]: # Present in machine-readable format
    for index, (title, url) in searchResults:
      echo title
      echo url
      echo ""
    quit(0)

  let numResults = min(limit, len(searchResults))

  present(searchResults, options, (0, numResults-1), player)


when isMainModule:
  main()
