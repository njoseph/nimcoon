import
  httpClient,
  json,
  re,
  strformat,
  strutils

let PEERTUBE_REGEX = re"w\/[0-9a-zA-z]{22}"

proc getPeerTubeMagnetLink*(url: string, musicOnly: bool): string =
  ## Gets the magnet link of the best possible resolution from PeerTube
  let uuid = url.substr(find(url, PEERTUBE_REGEX) + "w/".len)
  let domainName = url.substr(8, find(url, '/', start=8) - 1)
  let apiURL = &"https://{domainName}/api/v1/videos/{uuid}"
  let client = newHttpClient()
  let response = get(client, apiURL)
  let jsonNode = parseJson($response.body)
  var files = jsonNode["files"]
  if len(jsonNode["files"]) == 0:
    files = jsonNode["streamingPlaylists"][0]["files"]
  if musicOnly:
    files[len(files)-1]["magnetUri"].getStr()
  else:
    files[0]["magnetUri"].getStr()
