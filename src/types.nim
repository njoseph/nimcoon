import tables

type
  Options* = Table[string, bool]
  SearchResult* = tuple[title: string, url: string]
  SearchResults* = seq[tuple[title: string, url: string]]
  CommandLineOptions* = tuple[searchQuery: string, options: Options]
  SelectionRange* = tuple[begin: int, until: int]
