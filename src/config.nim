# Default configuration values

# Supported video players in order of preference.
# Should be able to play YouTube videos directly.
let supportedPlayers* = ["mpv", "vlc"]

# Only show these many results
let limit* = 10

# Download videos to this directory
let videoDownloadDirectory* = "~/Videos"

# Download music to this directory
let musicDownloadDirectory* = "~/Music"

# Rewrite Invidious URLs to YouTube
# Using Invidious as a proxy makes loading YouTube videos much slower
let rewriteInvidiousURLs* = true

# Invidious instance for querying
# This instance should have a valid public API
# Check like this: curl https://invidious.xyz/api/v1/search\?q\=cats
let invidiousInstance* = "https://invidious.snopyta.org"

discard """"
Configuration goes through three levels of overrides:

   /etc/nimcoon/config.json - configuration set by system administrator
   ~/.config/nimcoon/config.json   - per user configuration
   default configuration provided in this file
""""

# import os

# func getConfigFile(dir): string = getConfigDir() / "nimcoon" / "config.json"

# const ADMIN_CONFIGURATION = getConfigFile("etc")
# const USER_CONFIGURATION = getConfigFile(getConfigDir())

# const DEFAULT_CONFIGURATION = {
#   "entries_per_page": 10,
#   "video_download_directory": "~/Videos",
#   "music_download_directory": "~/Music",
#   "always_fullscreen": false,  # TODO not implemented yet
#   "rewrite_invidious_urls": false
# }.toTable  # toTable creates an immutable Table (newTable doesn't)
