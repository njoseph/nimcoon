# Package

version       = "0.9.0"
author        = "Joseph Nuthalapati"
description   = "A command-line player for YouTube, PeerTube and more."
license       = "GPL-3.0"
srcDir        = "src"
bin           = @["nimcoon"]

# Dependencies

requires "nim >= 1.0.2"
