;;; nimcoon.el -*- lexical-binding: t; -*-
;;;
;;; Commentary
;;; Usage in Doom Emacs
;;; Place or symlink the file into ~/.doom.d/
;;; (load! "nimcoon")

;;; Non-interactive functions to respond to URL clicks
(defun nimcoon-play-url (url &rest args)
  "Play given URL in Nimcoon."
  (start-process "nimcoon" nil "nimcoon" url))

;; Play all YouTube URLs in Nimcoon
(setq browse-url-browser-function
      (quote
       (("youtu\\.?be" . nimcoon-play-url)
        ("." . browse-url-default-browser))))

(defun run-nimcoon(args query)
  "Search by QUERY and play in Nimcoon."
  (call-process "nimcoon" nil 0 nil args query))

(defun nimcoon-search(args query)
  "Search by QUERY with the given ARGS."
  (with-output-to-temp-buffer "*Nimcoon search results*"
    (call-process "nimcoon" nil "*Nimcoon search results*" t args query)
    (with-current-buffer "*Nimcoon search results*"
      (org-mode))))

;;; Interactive functions
(defun nimcoon-feeling-lucky-music(query)
  (interactive "sSearch query: ")
  (run-nimcoon "-ml" query))

(defun nimcoon-feeling-lucky-video(query)
  (interactive "sSearch query: ")
  (run-nimcoon "-l" query))

(defun nimcoon-download-video(query)
  (interactive "sSearch query: ")
  (run-nimcoon "-dl" query))

(defun nimcoon-download-music(query)
  (interactive "sSearch query: ")
  (run-nimcoon "-dlm" query))

;; Assumes only one process exists. Must capture the pid of the running Nimcoon process and only kill it.
(defun nimcoon-kill-background-processes()
  "Kill Nimcoon process running in the background. Useful for stopping background music."
  (interactive)
  (shell-command "kill `pgrep nimcoon` `pgrep mpv` `pgrep vlc`"))

(defun nimcoon-search-video(query)
  "Search for a video by QUERY."
  (interactive "sSearch query: ")
  (nimcoon-search "-n" query))

(defun nimcoon-search-music(query)
  "Search for a video by QUERY."
  (interactive "sSearch query: ")
  (nimcoon-search "-nm" query))

;;; Keybindings
(map! :leader
      ;;; <leader> N --- Nimcoon
      (:prefix-map ("N" . "Nimcoon")
       (:prefix ("d" . "Download")
        :desc "Video" "v" #'nimcoon-download-video
        :desc "Music" "m" #'nimcoon-download-music)
       (:prefix ("s" . "Search")
        :desc "Video" "v" #'nimcoon-search-video
        :desc "Music" "m" #'nimcoon-search-music)
       :desc "Kill"  "k" #'nimcoon-kill-background-processes
       :desc "Video" "v" #'nimcoon-feeling-lucky-video
       :desc "Music" "m" #'nimcoon-feeling-lucky-music))

;;; nimcoon.el ends here
