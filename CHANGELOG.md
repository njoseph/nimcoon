# 0.9.0

- Replace youtube-dl with yt-dlp. Though this fixes the problem with YouTube throttling download speeds, it doesn't fix the throttling during streaming since mpv and vlc are still using youtube-dl. mpv has provided an option to provide an alternative youtube-dl implementation. Waiting for VLC to implement the same.
- Shift to PeerTube's new videoId format. Support for the UUID format is dropped.
- Dropped Peerflix as a dependency. WebTorrent is faster and better. They also added support for video player arguments on my request. See https://github.com/webtorrent/webtorrent-cli/issues/141

# 0.8.3

- Add fake user-agent

# 0.8.2

- Use a different Invidious instance.

# 0.8.1

- Use youtube-dl's ytsearch when feeling lucky. This make search much faster.

# 0.8.0

- Interactive options
- YouTube auto-play

# 0.7.0

- Fix broken direct play.
- Use Invidious instance for retrieving search results but use YouTube to get content.

# 0.6.1

- Minor bug fix

# 0.6.0

- Scrape from YouTube directly, no dependence on Invidious instances
- Doom Emacs integration

# 0.5.1

- Fix crash when search has <10 results
- Find alternative to invidio.us instance

# 0.5.0

- Use Invidio.us to retrieve search results
- Support http in addition to https
- README: Add section on Motivation
- README: Add comprehensive feature list
- Update README with a progress report on features
- Use aria2c download manager if available
- VLC is better than CVLC
- Minor formatting changes

# 0.4.0

- Allow picking up torrent links from http urls (PeerTube provides these in download options)
- Given a PeerTube link, automatically pick the best resolution video and stream it using WebTorrent

# 0.3.2

- Reimplement streaming of YouTube playlists

# 0.3.1

- Fix mistake with tagging.

# 0.3.0

- Add support for playing playlists of videos and songs from YouTube
- Playlists can be played by providing the first video in the list or by directly providing the playlist url

# 0.2.2

- Fix bug with paginated result selection

# 0.2.1

- Fix minor regression in pagination

# 0.2.0

- Implement pagination with shortcuts "n" and "p" to navigate between pages

# 0.1.0

- Search for videos using keywords
- Stream videos and music from YouTube
- Play direct links from YouTube and PeerTube
- Stream video and music from magnet links
- Download music
- Download video
