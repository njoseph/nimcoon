import
  tables,
  unittest

import lib

suite "Playing direct links":

  test "sanitize URL":
    let expected = "https://www.youtube.com/watch?v=QOEMv0S8AcA"
    check(sanitizeURL("https://youtu.be/QOEMv0S8AcA") == expected)
    check(sanitizeURL("https://www.youtube.com/watch\\?v\\=QOEMv0S8AcA") == expected)

  test "validate options":
    let invalidOptionsList = [
      to_table({"musicOnly": true, "feelingLucky": false, "fullScreen": true, "download": false, "autoPlay": false}),
      to_table({"musicOnly": false, "feelingLucky": true, "fullScreen": true, "download": true, "autoPlay": false}),
      # autoPlay download
      to_table({"musicOnly": true, "feelingLucky": true, "fullScreen": true, "download": true, "autoPlay": true}),
      # autoPlay video
      to_table({"musicOnly": false, "feelingLucky": true, "fullScreen": true, "download": false, "autoPlay": true}),
    ]
    for invalidOptions in invalidOptionsList:
      check(not isValidOptions(invalidOptions))

    let validOptionsList = [
      to_table({"musicOnly": false, "feelingLucky": true, "fullScreen": false, "download": true, "autoPlay": false}),
      to_table({"musicOnly": false, "feelingLucky": true, "fullScreen": true, "download": false, "autoPlay": false}),
      to_table({"musicOnly": true, "feelingLucky": true, "fullScreen": false, "download": false, "autoPlay": true}),
    ]
    for validOptions in validOptionsList:
      check(isValidOptions(validOptions))

  test "rewrite invidious urls":
    let url = "https://invidious.snopyta.org/watch?v=sZhxCUay5ks"
    check(rewriteInvidiousToYouTube(url) == "https://www.youtube.com/watch?v=sZhxCUay5ks")
